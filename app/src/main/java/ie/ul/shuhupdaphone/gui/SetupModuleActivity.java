package ie.ul.shuhupdaphone.gui;

import ie.ul.shuhupdaphone.R;
import ie.ul.shuhupdaphone.gui.Slot.CONTACT_TYPE;
import ie.ul.shuhupdaphone.gui.Slot.DAY;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

public class SetupModuleActivity extends Activity {

	private Button saveModuleButton;
	private Button addSlotButton;
	private TimePicker startTimePicker;
	private TimePicker endTimePicker;
	private Spinner daySpinner;
	private Spinner typeSpinner;
	private EditText moduleNameText;
	private EditText moduleCodeText;
	private Module newModule;
	private TextView scheduleView;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_module);
        
        /*
         * Exercise: all inits removed
         */
        saveModuleButton = (Button) findViewById(R.id.commitButton);
        addSlotButton = (Button) findViewById(R.id.addSlotButton);
        daySpinner = (Spinner) findViewById(R.id.daySpinner);
        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        startTimePicker = (TimePicker) findViewById(R.id.startTimePicker);
        endTimePicker = (TimePicker) findViewById(R.id.endTimePicker);
        moduleNameText = (EditText) findViewById(R.id.moduleName);
        moduleCodeText = (EditText) findViewById(R.id.moduleCode);
        scheduleView = (TextView) findViewById(R.id.scheduleView);
        
        ArrayAdapter<Slot.DAY> dayAdapter = new ArrayAdapter<Slot.DAY>(getBaseContext(), R.layout.list_item, Slot.DAY.values());
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        daySpinner.setAdapter(dayAdapter);
        
        ArrayAdapter<Slot.CONTACT_TYPE> typeAdapter = new ArrayAdapter<Slot.CONTACT_TYPE>(getBaseContext(), R.layout.list_item, Slot.CONTACT_TYPE.values());
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);    
        typeSpinner.setAdapter(typeAdapter);
        
        /*
         * Exercise: set start hour to 9 and end hour to 10
         */
        startTimePicker.setIs24HourView(true);
        startTimePicker.setCurrentHour(15);
        startTimePicker.setCurrentMinute(0);
        endTimePicker.setIs24HourView(true);
        endTimePicker.setCurrentHour(16);
        endTimePicker.setCurrentMinute(0);
        
        newModule = new Module();
        
        saveModuleButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * Exercise:  A module can be saved when the module name and code have been set and at least one schedule.
				 *  Save the current module if all three conditions are fulfilled
				 */
				if ((isModuleNameCompleted()) && (isModuleCodeCompleted()) && (isScheduleSet())) {
					save();
				}
			
			}
        });

        addSlotButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Slot newSlot = new Slot((DAY) daySpinner.getSelectedItem(), startTimePicker.getCurrentHour(), endTimePicker.getCurrentHour(), (CONTACT_TYPE) typeSpinner.getSelectedItem());
				newModule.addTimeSlot(newSlot);
				scheduleView.append(newModule.getTimeSlots(Slot.CONTACT_TYPE.ALL)[newModule.getTimeSlots(Slot.CONTACT_TYPE.ALL).length-1]+"\n");			
			}
        });
        
        startTimePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				/*
				 * Exercise: set end hour to 1 hour after start 
				 */
				endTimePicker.setCurrentHour(hourOfDay+1);
				
			}
		});
     }
	 
	 private boolean isModuleNameCompleted() {
		 if (moduleNameText.getText().toString().contentEquals(""))
			 return false;
		 return true;
	 }
	 
	 private boolean isModuleCodeCompleted() {
		 if (moduleCodeText.getText().toString().contentEquals(""))
			 return false;
		 return true;
	 }
	 
	 private boolean isScheduleSet() {
		 if (newModule.getNumberOfTimeSlots() > 0)
			 return true;
		 return false;
	 }
	 
	 /*
	  * For now a list of modules (with name, code and timeslots contained as field in the module object) is saved in the main activity
	  */
	 private void save() {
		 MainActivity.moduleList.add(newModule);
	 }
	 
}
