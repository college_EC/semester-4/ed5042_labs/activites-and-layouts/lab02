package ie.ul.shuhupdaphone.gui;

/* Student name: Eoghan Conlon
 * Student id: 21310262
 * Partner name: N/A
 * Partner id: N/A
 */

import ie.ul.shuhupdaphone.R;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	
	/*
	 * For now modules are stored in this moduleList. ArrayLists are ideal for this: the size does not need to be set apriori, 
	 * elements can be added (at the end or in the desired location) and removed as needed. For now this ArrayList will be 
	 * accessed from the other activities. Note that this is very dangerous as the list will be deleted in case the Activity
	 * is deleted! We do this here to keep things simple and in the future we will use a database for this. In a real app, you
	 * would NOT do this.
	 */
	public static ArrayList<Module> moduleList = new ArrayList<Module>();

    private Button newModuleButton;

    private Button viewScheduleButton;

    private Button addFullSchedule;

    private Button addLectureSlot;

    private Button addLabSlot;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_relative);

        newModuleButton = (Button) findViewById(R.id.newModuleButton);

        newModuleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getApplicationContext(), SetupModuleActivity.class);
                startActivity(newIntent);
            }
        });

        viewScheduleButton = (Button) findViewById(R.id.viewScheduleButton);

        viewScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(), ViewScheduleActivity.class);
                startActivity(newIntent);
            }
        });


        addFullSchedule = (Button) findViewById(R.id.addFullScheduleButton);

        addFullSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(), MainActivity.class); //Using main activity to test this button
                startActivity(newIntent);
            }
        });

        addLectureSlot = (Button) findViewById(R.id.addLectureSlotButton);

        addLectureSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(newIntent);
            }
        });

        addLabSlot = (Button) findViewById(R.id.addLabSlotButton);

        addLabSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(newIntent);
            }
        });
    }

   

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    
}
